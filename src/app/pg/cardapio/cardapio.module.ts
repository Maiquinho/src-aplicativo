import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { HttpModule } from '@angular/http';
import { CardapioPage } from './cardapio.page';

const routes: Routes = [
  {
    path: '',
    component: CardapioPage
  }
];

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, HttpModule, RouterModule.forChild(routes)],
  declarations: [CardapioPage]
})
export class CardapioPageModule {}
