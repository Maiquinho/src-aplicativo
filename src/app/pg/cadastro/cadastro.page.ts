import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validator, Validators} from '@angular/forms'; 
import {Http, Headers, Response} from '@angular/http'; 
import {map} from 'rxjs/operators'; 
import {UrlService} from '../../servidor/url.service';
import {NavController, } from '@ionic/angular'
import { headersToString } from 'selenium-webdriver/http';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {


  nome: any;
  email: any;
  senha: any;
  cadastro: any;

  constructor(public formConst: FormBuilder,
              public http: Http, 
              public nav: NavController,
              public servidorUrl: UrlService) {

                this.cadastro = this.formConst.group({
                  nome: ['', Validators.required], 
                  email: ['', Validators.required],
                  senha: ['', Validators.required],
                });



               }

cadCadastro(){
  if(this.nome == undefined || this.email == undefined || this.senha == undefined){
  this.servidorUrl.alertas('Atenção', 'Preencha todos os campos');
  }else{

this.cadDados(this.cadastro.value).subscribe(
  data => {
    console.log("Dados de teste - OK");
    this.servidorUrl.alertas('Thui tapioca' , this.nome + ', Seu cadastro foi efetuado com sucesso!')
    this.nav.navigateBack('tabs/pg/perfil');
  },
    err => {
      console.log("Erro de dados");
      this.servidorUrl.alertas('Thui Tapioca', 'Erro ao realizar o cadastro!')
    } 

    
  
)


    
  }
} //FIM METODO cadCADASTRO 

cadDados(nome){
  let cabecalho = new Headers({ 'content-type' : 'application/x-www-form-urlencoded'

    
  }); 
  return this.http.post(this.servidorUrl.pegarUrl()+'cadastro.php', nome, {
    headers: cabecalho,
    method: 'POST'
  }).pipe(map(
    (res: Response) => {return res.json();}
  ));

}




  ngOnInit() {
  }

}
