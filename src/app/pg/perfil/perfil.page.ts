import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../servidor/usuario.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  constructor(public dadosUser: UsuarioService) { 

    console.log(this.dadosUser.idUsuario);
    console.log(this.dadosUser.nomeUsuario);
  }

  ngOnInit() {
  }

}
