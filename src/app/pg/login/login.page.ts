import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { map, subscribeOn } from 'rxjs/operators';
import { UrlService } from '../../servidor/url.service';
import{UsuarioService}from '../../servidor/usuario.service';
import { NavController, AlertController, LoadingController } from '@ionic/angular/';
import { from } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  email: string;
  senha: string;

  constructor(
    public http: Http,
    public servidorUrl: UrlService,
    public dadosUser: UsuarioService,
    public nav: NavController,
    public alert: AlertController,
    public loading: LoadingController
  ) {
    this.email = 'joao@gmail.com';
    this.senha = '1234';
  }

  ngOnInit() {
    document.querySelector('ion-tab-bar').style.display = 'none';
  }
  logar() {
    if (this.email == undefined || this.senha == undefined) {
      console.log('Preencha todos os campos');
      this.servidorUrl.alertas('Atenção', 'Preencha todos os campos');
    } else {
      this.http
        .get(this.servidorUrl.pegarUrl() + 'login.php?email=' + this.email + '&senha=' + this.senha)
        .pipe(map(res => res.json()))
        .subscribe(data => {
          if (data.msg.Logado == 'sim') {
            if (data.dados.status == 'ATIVO') {
              /*Alimentar o SET do usuario*/



              this.nav.navigateBack('tabs/pg/perfil');
              console.log('Logado');
            } else {
              console.log('Usuário bloqueado');
              this.servidorUrl.alertas('Atenção', 'Usuário bloqueado');
            }
          } else {
            console.log('Usuário ou senha incorreto');
            this.servidorUrl.alertas('Atenção', 'Usuário ou senha incorreto');
          }
        });
    }
  }
}
